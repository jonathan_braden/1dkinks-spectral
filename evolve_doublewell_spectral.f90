
#define LAPLACE
#define DVDPHI

program lattice
  use fftw3

  implicit none

  integer, parameter :: dl = kind(1.d0)
  real*8, parameter :: twopi = 6.2831853071795864769252867665590
! Time Stepping Properties
  integer :: j, jj
  integer, parameter :: nstep = 2**10
  integer, parameter :: stepsize=2**5
  real(dl), parameter :: tstep = 0.0125

! Lattice Properties
  integer, parameter :: nlat = 512 !8192*2
  real(dl), parameter :: len = 64.
  real(dl), parameter :: dx = len/dble(nlat)
  real(dl), parameter :: dk = twopi / len

!
! Pack the phase space variables
!
  integer, parameter :: nfld=1
  integer, parameter :: nvar = 2*nfld*nlat
  real(dl), parameter :: del = 0.

  real(dl), dimension(nvar) :: yvec

  real(C_DOUBLE), pointer :: laplace(:)
  complex(C_DOUBLE_COMPLEX), pointer :: Fk(:)
  

! Preprocessor macros for identifying individual field components
#define FLD yvec(1:nlat)
#define FLDP yvec(nlat+1:2*nlat)
#define AVAR yvec(2*nlat+1)
#define PIA yvec(2*nlat+2))

! Initialize arrays for doing spectral laplacian
  call allocate_1d_array(nlat, laplace, Fk)

  open(unit=99,file='field_values_spec.out')
  open(unit=98,file='energy_spec.out')

  call init_fields(3.*2.**0.5,0.)
  call write_fields(0.)
  call dump_rho(0.)

  do j=1,nstep
     do jj=1, stepsize
        call gl10( yvec(:), tstep )
     enddo
     call write_fields(j*stepsize*tstep)
     call dump_rho(j*stepsize*tstep)
  enddo

  contains
!
! 10th order Gauss-Legendre integrator
! Used for pieces of Hamiltonian without exact solutions
! To do : implement entire evolution using this
! To do : pack into smaller vector as required (just chop a, so probably not worth it)
!
!
! Hmm, need to sort out the packing of the vector for this to work (c.f. matmul's that appear)
!
    subroutine gl10( y, dt )
      real*8 :: y(nvar)
      real*8 :: dt

      integer, parameter :: s = 5
      real*8 :: g(nvar, s)

      ! Butcher tableau for 8th order Gauss-Legendre method
      real*8, parameter :: a(s,s) = reshape( (/ &
           0.5923172126404727187856601017997934066D-1, -1.9570364359076037492643214050884060018D-2, &
           1.1254400818642955552716244215090748773D-2, -0.5593793660812184876817721964475928216D-2, &
           1.5881129678659985393652424705934162371D-3,  1.2815100567004528349616684832951382219D-1, &
           1.1965716762484161701032287870890954823D-1, -2.4592114619642200389318251686004016630D-2, &
           1.0318280670683357408953945056355839486D-2, -2.7689943987696030442826307588795957613D-3, &
           1.1377628800422460252874127381536557686D-1,  2.6000465168064151859240589518757397939D-1, &
           1.4222222222222222222222222222222222222D-1, -2.0690316430958284571760137769754882933D-2, &
           4.6871545238699412283907465445931044619D-3,  1.2123243692686414680141465111883827708D-1, &
           2.2899605457899987661169181236146325697D-1,  3.0903655906408664483376269613044846112D-1, &
           1.1965716762484161701032287870890954823D-1, -0.9687563141950739739034827969555140871D-2, &
           1.1687532956022854521776677788936526508D-1,  2.4490812891049541889746347938229502468D-1, &
           2.7319004362580148889172820022935369566D-1,  2.5888469960875927151328897146870315648D-1, &
           0.5923172126404727187856601017997934066D-1 /) , [s,s])
      real, parameter :: b(s) = (/ &
           1.1846344252809454375713202035995868132D-1,  2.3931433524968323402064575741781909646D-1, &
           2.8444444444444444444444444444444444444D-1,  2.3931433524968323402064575741781909646D-1, &
           1.1846344252809454375713202035995868132D-1 /)

      integer :: i,k

      g = 0.
      do k=1,16
#ifdef USEBLAS
         call DGEMM('N','N', nvar, s, s, 1., g, nvar, a, s, 0., g,nvar)
#else
         g = matmul(g,a)
#endif
         do i=1,s
            call derivs( y+g(:,i)*dt , g(:,i) )
         enddo
      enddo
#ifdef USEBLAS
      call DGEMV('N','N', nvar,s, dt,g,nvar, b,1 ,1.,y,1)
#else
      y = y + matmul(g,b)*dt
#endif
    end subroutine gl10

    subroutine gl6(y, dt)
      integer, parameter :: s=3

      real*8 :: y(nvar), g(nvar,s), dt
      integer :: i,k

      ! Butcher tableu for 6th-order integrator
      real*8, parameter :: a(s,s) = reshape( (/ &
           5./36.0, 2.0/9.0 - 1./sqrt(15.0), 5.0/36.0 - 0.5/sqrt(15.0), &
           5.0/36.0 + sqrt(15.0)/24.0, 2.0/9.0, 5.0/36.0-sqrt(15.0)/24.0, &
           5.0/36.0 + 0.5/sqrt(15.0), 2.0/9.0+1.0/sqrt(15.0), 5.0/36.0 &
           /), [s,s])
      real*8, parameter :: b(s) = (/ 5.0/18.0, 4.0/9.0, 5.0/18.0 /)

      g = 0.
      do k=1,16
         g = matmul(g,a)
         do i=1,s
            call derivs(y + g(:,i)*dt, g(:,i))
         enddo
      enddo

      y = y + matmul(g,b)*dt
    end subroutine gl6

    subroutine write_fields(time)
      integer :: i
      real(dl) :: time

      do i=1,nlat
         write(99,*) time, i*dx-len/2., yvec(i), yvec(nlat+i)
      enddo
      write(99,*)
    end subroutine write_fields

!
! Evolution vector in phase space for GL integration
!
    subroutine derivs(yc, yp)
      real*8, dimension(1:nvar) :: yc, yp

      integer :: i
      real :: lnorm

      lnorm = 1./dx**2

! Currently first nlat variables in the vector are the phis, the next nlat are the pi's 

! dphi/dt
      yp(1:nlat) = yc(nlat+1:2*nlat)
! dpi/dt
      laplace(:) = yc(1:nlat)
      call laplacian_1d(nlat, laplace, Fk, dk)
      yp(nlat+1:2*nlat) = -yc(1:nlat)*(yc(1:nlat)**2-1.) + del + laplace(1:nlat)
    end subroutine derivs
!
! Model potential and derivative
!
    real(dl) function modelv(f)
      real(dl), dimension(1:nfld) :: f      

      modelv = 0.25*(f(1)**2-1.)**2 - del*(f(1)-1.)
      return
    end function modelv

    function modeldv(f)
      real(dl), dimension(1:nfld) :: modeldv
      real(dl), dimension(1:nfld) :: f
      real(dl) :: denom
      
      modeldv(1) = f(1)*(f(1)**2-1.) - del
      return
    end function modeldv

    function modeld2v(f)
      real(dl), dimension(1:nfld,1:nfld) :: modeld2v
      real(dl), dimension(1:nfld) :: f

      modeld2v(1,1) = 3.*f(1)**2 - 1.
    end function modeld2v

    subroutine init_fields(rinit, vinit)
      real(dl) :: rinit, vinit

      real(dl) :: phitrue, phifalse
      phifalse=-1.
      call find_vacuum(phifalse)
      phitrue=1.
      call find_vacuum(phitrue)

      print*,"Vacua are ",phifalse, phitrue

      call make_kink(len/2.-rinit,phitrue,phifalse,2.**0.5,vinit,.false.)
      call make_kink(len/2.+rinit,phifalse,phitrue,2.**0.5,-vinit,.true.)
      yvec(1:nlat) = yvec(1:nlat) - phifalse
    end subroutine init_fields

    subroutine make_kink(x0, phileft, phiright, width, speed, superpose)
      real(dl) :: speed, phileft, phiright, x0, width
      logical :: superpose

      integer :: i
      real(dl) :: phistep, phiave, gamma
      real(dl) :: ftmp, fptmp, xcur

      phistep = 0.5*(phiright - phileft)
      phiave = 0.5*(phiright + phileft)
      gamma = 1. / (1.-speed**2)**0.5

      do i=1,nlat
         xcur=i*dx-x0
         ftmp = phistep*tanh(xcur*gamma/width) + phiave
         fptmp = -phistep*(speed*gamma/width) / cosh(gamma*xcur/width)**2
         if (superpose) then
            yvec(i) = yvec(i) + ftmp
            yvec(nlat+i) = yvec(nlat+i) + fptmp
         else
            yvec(i) = ftmp
            yvec(nlat+i) = fptmp
         endif
      enddo
    end subroutine make_kink
    
    subroutine dump_rho(time)
      real(dl) :: time
      integer :: i

      real(dl) :: GE, PE, KE, rho, mom

! This loop can be shortened, as I'm double summing a lot of things
!      GE = 0.
!      GE = sum( (yvec(2:nlat)-yvec(1:nlat-1))**2 )
!      GE = GE + (yvec(nlat)-yvec(1))**2
!      GE = 0.5*GE / dx**2 / nlat  ! This is 0.5 not 0.25 since there is an overall factor of 2 on all the above stuff

      laplace = yvec(1:nlat)
      GE = grad_energy_1d(nlat, laplace, Fk, dk)
!      PE = 0.25*sum((yvec(1:nlat)**2-1.)**2) / nlat
      PE = sum( 0.25*(yvec(1:nlat)**2-1.)**2 - del*(yvec(1:nlat)-1.) ) / nlat
      KE = 0.5*sum(yvec(nlat+1:2*nlat)**2) / nlat

      laplace = yvec(1:nlat)
      call derivative_1d(nlat, laplace, Fk, dk)
      mom = sum(yvec(nlat+1:2*nlat)*laplace(1:nlat))
      mom = mom / nlat
      
      rho = KE + PE + GE
      
      write(98,*) time, rho, KE, PE, GE, mom
    end subroutine dump_rho
    
! To do, adjust this for arbitrary number of fields
    subroutine find_vacuum(fld)
      real(dl), intent(inout) :: fld

      integer :: i,j
      real(dl) :: dfld, ddfld
      integer, parameter :: maxit=16
      real(dl), parameter :: min_tol = 1.e-14

      do i=1,maxit
         ddfld = 3.*fld**2-1.
         dfld = -fld*(fld**2-1.) + del
         if (abs(dfld) < min_tol) exit
         fld = fld + dfld/ddfld
      enddo

      if (i.eq.maxit) then
         print*,"Failed to find local minimum of potential.  Adjust initial guess.  Quitting."
         stop
      endif

      print*,"Field derivative at proposed minimum ",dfld
    end subroutine find_vacuum

  end program lattice
