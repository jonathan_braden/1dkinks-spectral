import numpy as np
import matplotlib.pyplot as plt

infile=""
nlat=

a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(1,(-1,nlat))
a=np.genfromtxt(infile,usecols=[])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-[nlat/2-1]

eps=0.05
cvals=np.arange(-1.2,1.2+eps,eps)
plt.contourf(xvals,tvals,fld,cvals,cmap=plt.cm.RdYlBu)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\frac{\phi}{\phi_0}$', fontsize=24,rotation='horizontal')

