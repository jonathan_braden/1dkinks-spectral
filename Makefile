FC=gfortran
FFLAGS=-fdefault-real-8 -fdefault-double-8 -cpp -ffree-line-length-none -fopenmp
FWARN= #-Wall -fbounds-check
FOPT=-march=native -flto -funroll-loops -O3 #-fblas  (check this blas one to replace matmul calls)
FPROF= #-g -pg

#Intel Compiler
#FC=ifort
#FFLAGS=-r8 -fpp
#FOPT=-O3 -fast -xHost -parallel -ipo -no-prec-div
#FPROF=

FFTW_INC=-I/usr/local/fftw-3.3.3/include/
FFTW_LIB=-L/usr/local/fftw-3.3.3/lib

FFTW = -lfftw3_omp -lfftw3

#ifdef OMP
#FFLAGS+= -fopenmp
#FFTW+= -lfftw3_threads -lfftw3
#endif

#ifdef THREADS
#THREAD_LIB = -lpthread
#FFTW = -lfftw3_threads -lfftw3

OBJS = fftw_mod.o

spectral: %: $(OBJS) evolve_doublewell_spectral.o
	$(FC) $(FFTW_INC) $(FFLAGS) $(FOPT) $(FPROF) $(FWARN) -o runspec evolve_doublewell_spectral.f90 $(OBJS) $(FFTW_LIB) $(FFTW) -lm $(THREAD_LIB)

bubble: %: $(OBJS) evolve_bubble_spectral.o
	$(FC) $(FFTW_INC) $(FFLAGS) $(FOPT) $(FPROF) $(FWARN) -o runbubble evolve_bubble_spectral.f90 $(OBJS) $(FFTW_LIB) $(FFTW) -lm $(THREAD_LIB)

%.o: %.f90
	$(FC) $(FFLAGS) $(FFTW_INC) $(FOPT) $(FPROF) $(FWARN) -c $< -o $@ $(FFTW_LIB) $(FFTW) -lm $(THREAD_LIB)

clean:
	rm -f *.o
	rm -f *.mod
	rm -f runspec