import numpy as np

infiles=["en_l1024_n2048_dt0.4.dat","en_l1024_n2048_dt0.2.dat", "en_l1024_n2048_dt0.1.dat","en_l1024_n2048_dt0.05.dat","en_l1024_n2048_dt0.025.dat"]#,"en_l1024_n2048_dt0.0125.dat"]
labels=[r'$dt=4dx/5$',r'$dt=2dx/5$',r'$dt=dx/5$',r'$dt=dx/10$',r'$dt=dx/20$']

tvals=[]
evals=[]
pvals=[]
for fname in infiles:
    a=np.genfromtxt(fname,usecols=[0,1,5])
    etmp=a[:,1]
    etmp=etmp-etmp[0]
    evals.append(etmp)
    tvals.append(a[:,0])
    pvals.append(a[:,2])

import matplotlib.pyplot as plt
for i in range(len(evals)):
    plt.plot(tvals[i],np.abs(evals[i]),label=labels[i])

plt.xlabel(r'$mt$')
plt.ylabel(r'$|\rho-\rho_0$|')
plt.yscale('log')
plt.xlim(0,400)
plt.legend()

plt.show()

for i in range(len(pvals)):
    plt.plot(tvals[i],np.abs(pvals[i]),label=labels[i])

plt.xlabel(r'$mt$')
plt.ylabel(r'$|\langle\phi\partial_x\phi\rangle|$')
plt.xlim(0,400)
plt.legend(loc='upper left')
