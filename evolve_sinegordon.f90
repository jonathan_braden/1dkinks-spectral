!
! Module to store the desired evolution scheme for the fields, etc.
!

!
! To do : Add packing and unpacking of phase space variables
!

!
! To do: Add the Minkowski evolution and fixed background evolution
!   make sure I have the macros in the right place
!

#define LAPLACE
#define DVDPHI

program lattice
  implicit none

  integer, parameter :: dl = kind(1.d0)

! Time Stepping Properties
  integer :: j, jj
  integer, parameter :: nstep = 2**8
  integer, parameter :: stepsize=2**0
  real(dl), parameter :: tstep = 0.05

! Lattice Properties
  integer, parameter :: nlat = 256
  real(dl), parameter :: len = 32.
  real(dl), parameter :: dx = len/dble(nlat)

!
! Pack the phase space variables
!
  integer, parameter :: nfld=1
  integer, parameter :: nvar = 2*nfld*nlat ! + 2

  real(dl), dimension(nvar) :: yvec

! Preprocessor macros for identifying individual field components
#define FLD yvec(1:nlat)
#define FLDP yvec(nlat+1:2*nlat)
#define AVAR yvec(2*nlat+1)
#define PIA yvec(2*nlat+2))

  open(unit=99,file='field_values.out')
  open(unit=98,file='energy.out')

  call init_fields(0.01)
  call write_fields(0.)
  call dump_rho(0.)

  do j=1,nstep
     do jj=1, stepsize
        call gl10( yvec(:), tstep )
     enddo
     call write_fields(j*stepsize*tstep)
     call dump_rho(j*stepsize*tstep)
  enddo

  contains
!
! 10th order Gauss-Legendre integrator
! Used for pieces of Hamiltonian without exact solutions
! To do : implement entire evolution using this
! To do : pack into smaller vector as required (just chop a, so probably not worth it)
!
!
! Hmm, need to sort out the packing of the vector for this to work (c.f. matmul's that appear)
!
! Warning : The convergence rate of the iterations seems to depend on dx
!
    subroutine gl10( y, dt )
      real*8 :: y(nvar)
      real*8 :: dt

      integer, parameter :: niter = 16

      integer, parameter :: s = 5
      real*8 :: g(nvar, s)

      ! Butcher tableau for 10th order Gauss-Legendre method
      real*8, parameter :: a(s,s) = reshape( (/ &
           0.5923172126404727187856601017997934066D-1, -1.9570364359076037492643214050884060018D-2, &
           1.1254400818642955552716244215090748773D-2, -0.5593793660812184876817721964475928216D-2, &
           1.5881129678659985393652424705934162371D-3,  1.2815100567004528349616684832951382219D-1, &
           1.1965716762484161701032287870890954823D-1, -2.4592114619642200389318251686004016630D-2, &
           1.0318280670683357408953945056355839486D-2, -2.7689943987696030442826307588795957613D-3, &
           1.1377628800422460252874127381536557686D-1,  2.6000465168064151859240589518757397939D-1, &
           1.4222222222222222222222222222222222222D-1, -2.0690316430958284571760137769754882933D-2, &
           4.6871545238699412283907465445931044619D-3,  1.2123243692686414680141465111883827708D-1, &
           2.2899605457899987661169181236146325697D-1,  3.0903655906408664483376269613044846112D-1, &
           1.1965716762484161701032287870890954823D-1, -0.9687563141950739739034827969555140871D-2, &
           1.1687532956022854521776677788936526508D-1,  2.4490812891049541889746347938229502468D-1, &
           2.7319004362580148889172820022935369566D-1,  2.5888469960875927151328897146870315648D-1, &
           0.5923172126404727187856601017997934066D-1 /) , [s,s])
      real, parameter :: b(s) = (/ &
           1.1846344252809454375713202035995868132D-1,  2.3931433524968323402064575741781909646D-1, &
           2.8444444444444444444444444444444444444D-1,  2.3931433524968323402064575741781909646D-1, &
           1.1846344252809454375713202035995868132D-1 /)

      integer :: i,k

      g = 0.
      do k=1,niter
         g = matmul(g,a)
         do i=1,s
            call derivs( y+g(:,i)*dt , g(:,i) )
         enddo
      enddo
      y = y + matmul(g,b)*dt

    end subroutine gl10

    subroutine write_fields(time)
      integer :: i
      real(dl) :: time

      do i=1,nlat
         write(99,*) time, i*dx, yvec(i), yvec(nlat+i)
      enddo
      write(99,*)

    end subroutine write_fields

!
! Evolution vector in phase space for GL integration
!
    subroutine derivs(yc, yp)
      real*8, dimension(1:nvar) :: yc, yp

      integer :: i
      real :: lnorm

      lnorm = 1./dx**2

! Currently first nlat variables in the vector are the phis, the next nlat are the pi's 

! dphi/dt
      yp(1:nlat) = yc(nlat+1:2*nlat)
! dpi/dt
      do i=2,nlat-1
         yp(i+nlat) = -sin(yc(i)) + lnorm*(yc(i+1) + yc(i-1) - 2.*yc(i) )
      enddo
!      yp(nlat+2:2*nlat-1) = -sin(yc(2:nlat-1)) + lnorm*( yc(3:nlat) + yc(1:nlat-2) - 2.*yc(2:nlat-1) )
      yp(nlat+1) = -sin(yc(1)) + lnorm*(yc(nlat) + yc(2) - 2.*yc(1))
      yp(2*nlat) = -sin(yc(nlat)) + lnorm*(yc(1) + yc(nlat-1) - 2.*yc(nlat))
    end subroutine derivs
!
! Model potential and derivative
!
    real(dl) function modelv(f)
      real(dl), dimension(1:nfld) :: f
      
      modelv = 0.5*f(1)**4
      return
    end function modelv

    function modeldv(f)
      real(dl), dimension(1:nfld) :: modeldv
      real(dl), dimension(1:nfld) :: f
      real(dl) :: denom
      
      modeldv(1) = f(1)**3
      return
    end function modeldv

    subroutine init_fields(vparam)
      real(dl) :: vparam

      integer :: i
      real(dl) :: x, x0, ptemp, gamma

      gamma = 1. / (1.+vparam**2)**0.5
      x0 = len/2.
      do i=1,nlat
         x=i*dx-x0
         ptemp = vparam*cosh(gamma*x)
         yvec(i) = 4.*atan(1./ptemp)
         yvec(nlat+i) = 0.
      enddo
    end subroutine init_fields

    subroutine dump_rho(time)
      real(dl) :: time
      integer :: i

      real(dl) :: GE, PE, KE, rho

! This loop can be shortened, as I'm double summing a lot of things
      GE = 0.
      GE = sum( (yvec(2:nlat)-yvec(1:nlat-1))**2 )
      GE = GE + (yvec(nlat)-yvec(1))**2
      GE = 0.5*GE / dx**2 / nlat  ! This is 0.5 not 0.25 since there is an overall factor of 2 on all the above stuff

      PE = -sum(cos(yvec(1:nlat))) / nlat
      KE = 0.5*sum(yvec(nlat+1:2*nlat)**2) / nlat
      
      rho = KE + PE + GE
      
      write(98,*) time, rho, KE, PE, GE
    end subroutine dump_rho

! Check Hamiltonian constraint
!    real*8 function evalh(fv)
!      real*8, dimension(2*(nfld+1)) :: fv

!      real*8, dimension(nfld,nfld) :: gi
!      gi = ginv(fv(1:2))
      
!#ifdef CONFORMAL
!      evalh = fv(2*(nfld+1))**2/12. - 0.5 * ( fv(3)**2+fv(4)**2 )/fv(2*nfld+1)**2 - 0.5*fv(2*nfld+1)**4*modelv(fv(1:2))
!      evalh = fv(2*(nfld+1))**2/12.   &
!             - 0.5 * ( fv(3)**2*gi(1,1) + fv(4)**2*gi(2,2) )/fv(2*nfld+1)**2  &
!             - 0.5*fv(2*nfld+1)**4*modelv(fv(1:2))
!#else  ! fill this in for cosmic time
!      evalhh = 0.
!#endif
!
!    end function evalh
    
  end program lattice
