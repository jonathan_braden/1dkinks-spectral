import numpy as np

infiles=["field_l1024_n2048_dt0.4.dat","field_l1024_n2048_dt0.2.dat", "field_l1024_n2048_dt0.1.dat","field_l1024_n2048_dt0.05.dat","field_l1024_n2048_dt0.025.dat","field_l1024_n2048_dt0.0125.dat"]
latsize=[2048,2048,2048,2048,2048,2048]
labels=[r'$dt_i=dx/$',r'$dt_i=dx/$',r'$dt_i=dx/$',r'$dt_i=dx/$',r'$dt_i=dx/$']
colors=['b','r','g','k','c']
nmin=1024

fields=[]
tvals=[]
for i in range(len(infiles)):
    fname=infiles[i]
    nsize=latsize[i]
    ds=nsize/nmin
    a=np.genfromtxt(fname,usecols=[2])
    ftmp=np.reshape(a,(-1,nsize))
    fields.append(ftmp[:,ds-1::ds])
    a=np.genfromtxt(fname,usecols=[0])
    tvals.append(a[0::latsize[0]])

# Adjust these comparisons as needed given the input list
diffs=[]
for i in range(len(fields)-1):
    diffs.append(fields[i+1]-fields[i])

norm=[]
for i in range(len(diffs)):
    ntmp=[]
    for j in range(len(diffs[i])):
        ntmp.append(np.sum(np.abs(diffs[i][j])))
    norm.append(ntmp)
norm=np.array(norm)

nmax=[]
for i in range(len(diffs)):
    ntmp=[]
    for j in range(len(diffs[i])):
        ntmp.append(np.max(np.abs(diffs[i][j])))
    nmax.append(ntmp)
nmax=np.array(nmax)

import matplotlib.pyplot as plt

for i in range(len(nmax)):
    plt.plot(tvals[i],norm[i],colors[i]+'-',label=labels[i])
    plt.plot(tvals[i],nmax[i],colors[i]+'-.')

plt.yscale('log')
plt.xlim((0,400))
plt.xlabel(r'$mt$',fontsize=16)
plt.ylabel(r'$\frac{1}{N}\sum_{x_i}|\phi^{dt/2}(x_i)-\phi^{dt}(x_i)|, \mathrm{max}(|\phi^{dt/2}(x_i)-\phi^{dt}(x_i)|)$',fontsize=16)
plt.legend(loc='lower right')
